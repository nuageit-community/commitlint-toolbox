FROM public.ecr.aws/docker/library/node:lts-alpine3.17

RUN npm install --location=global \
        @commitlint/cli@16.1.0 \
        @commitlint/config-conventional@16.0.0

ENTRYPOINT [ "commitlint", "--edit" ]
